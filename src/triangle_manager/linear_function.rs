use sdl2::rect::Point;

#[derive(Debug)]
pub struct LinearFunction {
    slope: f32,
    x_of_y_zero: f32,
}

impl LinearFunction {
    pub fn new(point_1: Point, point_2: Point) -> LinearFunction {
        // calc slope and x_zero form two pints which fullfill the linear equation
        // slpoe can be infinite in case devided by zero due to vertical line
        let mut slope_: f32;
        let x_of_y_zero_: f32;
        let (point_a, point_b): (Point, Point);

        if (point_1.x() > point_2.x()) {
            point_a = point_1;
            point_b = point_2;
        } else {
            point_a = point_2;
            point_b = point_1;
        }

        if point_a.x() == point_b.x() {
            // horizontal line
            x_of_y_zero_ = point_a.x() as f32;
            slope_ = std::f32::INFINITY;
        } else if point_a.y() == point_b.y() {
            // vertival line
            slope_ = 0.0;
            x_of_y_zero_ = point_a.y() as f32;
        } else {
            slope_ = -((point_a.y() - point_b.y()) as f32 / (point_a.x() - point_b.x()) as f32);
            x_of_y_zero_ = point_a.y() as f32 - slope_ * point_a.x() as f32;
        }

        println!(
            "slope = {}, point_a = {:?}, point_b = {:?}, diff_y = {}, diff_x = {},
 x_of_y_zero {}",
            slope_,
            point_a,
            point_b,
            point_a.y() - point_b.y(),
            point_a.x() - point_b.x(),
            x_of_y_zero_
        );

        LinearFunction {
            slope: slope_,
            x_of_y_zero: x_of_y_zero_,
        }
    }

    pub fn check_if_point_beneath_line(&self, x: i32, y: i32) -> bool {
        let mut result: f32 = 0.0;
        if self.slope != std::f32::INFINITY && self.slope != 0.0 && self.slope != std::f32::NAN {
            result = y as f32 - self.slope * x as f32 + self.x_of_y_zero;
        } else {
            return true;
        }

        //println!(" result = {}", result);

        if result > 0.0 {
            return true;
        }

        false
    }
}
