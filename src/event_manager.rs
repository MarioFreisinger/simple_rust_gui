// manage events like keystrokes ...
extern crate sdl2;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

fn run(event_pump: *sdl2::EventPump) {
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,

                _ => (),
            }
        }
    }
}
