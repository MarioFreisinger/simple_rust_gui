extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;

mod triangle_manager;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem
        .window("rust-sdl2 demo", 800, 600)
        .position_centered()
        .build()
        .unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut canvas = window.into_canvas().build().unwrap();

    let mut triangle_manager = triangle_manager::TriangleManager::new(&mut canvas);

    // quit in exit or esscape
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                //handle Quit
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,

                // draw rectangel
                Event::KeyDown {
                    keycode: Some(Keycode::D),
                    ..
                } => triangle_manager.draw_triangles(),

                //randomize vertices of triangles
                Event::KeyDown {
                    keycode: Some(Keycode::R),
                    ..
                } => triangle_manager.randomize_vertices(),

                // rendomize vertices of rectangle
                _ => (),
            }
        }
    }
}
