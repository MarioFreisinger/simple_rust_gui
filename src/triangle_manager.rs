use linear_function::LinearFunction;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::render::Canvas;
use sdl2::video::Window;

mod linear_function;

//zero for single threaded scanline and one for multithreaded parallel fill
const SCANLINE_OR_PARALLEL_FILL: bool = true;

pub struct TriangleManager<'a> {
    vertices: Vec<Point>,
    canvas: &'a mut Canvas<Window>,
}

impl<'a> TriangleManager<'a> {
    // init struct data
    pub fn new(canvas_: &mut Canvas<Window>) -> TriangleManager {
        canvas_.set_draw_color(Color::RGB(0, 0, 0));
        canvas_.clear();
        canvas_.present();
        canvas_.set_draw_color(Color::RGB(255, 255, 255));

        TriangleManager {
            vertices: Vec::new(),
            canvas: canvas_,
        }
    }

    pub fn randomize_vertices(&mut self) {
        //static vertices for now
        let mut point_a = Point::new(100, 100);
        let mut point_b = Point::new(400, 100);
        let mut point_c = Point::new(400, 400);

        self.vertices.push(point_a);
        self.vertices.push(point_b);
        self.vertices.push(point_c);

        /*
        for i in 0..3 {
            self.vertices.push(point);
            if i == 1 {
                point = point.offset(0, -40);
            } else {
                point = point.offset(40, 60);
            }
        }
         */
    }

    pub fn draw_triangles(&mut self) {
        for curr_point in &self.vertices {
            self.canvas.draw_point(*curr_point).unwrap();
        }

        if !SCANLINE_OR_PARALLEL_FILL {
            self.fill_triangles_scanline();
        } else {
            self.fill_triangles_parallel_fill();
        }

        self.canvas.present();
    }

    fn fill_triangles_parallel_fill(&mut self) {
        let lin_funcs_of_triangle = self.get_lin_funcs_of_edges();

        let (y_min, y_max, x_min, x_max) = self.get_maxima_minima_of_triangle();

        for y in y_min..y_max {
            for x in x_min..x_max {
                if !lin_funcs_of_triangle.0.check_if_point_beneath_line(x, y) {
                    continue;
                }

                if !lin_funcs_of_triangle.1.check_if_point_beneath_line(x, y) {
                    continue;
                }
                if !lin_funcs_of_triangle.2.check_if_point_beneath_line(x, y) {
                    continue;
                }

                let point = Point::new(x, y);
                self.canvas.draw_point(point).unwrap();
            }
        }
    }

    fn get_lin_funcs_of_edges(&self) -> (LinearFunction, LinearFunction, LinearFunction) {
        let lin_func_a_b = linear_function::LinearFunction::new(self.vertices[0], self.vertices[1]);

        let lin_func_b_c = linear_function::LinearFunction::new(self.vertices[1], self.vertices[2]);

        let lin_func_c_a = linear_function::LinearFunction::new(self.vertices[2], self.vertices[0]);

        (lin_func_a_b, lin_func_b_c, lin_func_c_a)
    }

    fn fill_triangles_scanline(&mut self) {
        let three_lin_funcs_of_triangle = self.get_lin_funcs_of_edges();

        #[cfg(test)]
        println!("{:?}", three_lin_funcs_of_triangle.0);
        println!("{:?}", three_lin_funcs_of_triangle.1);
        println!("{:?}", three_lin_funcs_of_triangle.2);
    }

    fn get_maxima_minima_of_triangle(&self) -> (i32, i32, i32, i32) {
        // lowest and highest y
        let mut lowest_y: i32 = self.vertices[0].y();
        let mut highest_y: i32 = self.vertices[0].y();

        //lowest and highest x
        let mut lowest_x: i32 = self.vertices[0].x();
        let mut highest_x: i32 = self.vertices[0].x();

        // using slice  without first vertex cuz already set to that
        for (i, &curr_vec) in self.vertices[1..3].iter().enumerate() {
            if curr_vec.y() < lowest_y {
                lowest_y = curr_vec.y();
            }

            if curr_vec.x() < lowest_x {
                lowest_x = curr_vec.x();
            }

            if curr_vec.y() > highest_y {
                highest_y = curr_vec.y();
            }

            if curr_vec.x() > highest_x {
                highest_x = curr_vec.x();
            }
        }

        (lowest_y, highest_y, lowest_x, highest_x)
    }
}

fn find_unused_vertex(used_vert_a: usize, used_vert_b: usize) -> usize {
    if used_vert_a != 0 && used_vert_b != 0 {
        return 0;
    }
    if used_vert_a != 1 && used_vert_b != 1 {
        return 1;
    }
    if used_vert_a != 2 && used_vert_b != 2 {
        return 2;
    }
    panic!("no unused vertex not possible");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rand_and_draw_triangles() {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();
        let window = video_subsystem
            .window("rust-sdl2 demo", 800, 600)
            .position_centered()
            .build()
            .unwrap();

        let mut canvas = window.into_canvas().build().unwrap();

        let mut triangle_manager = TriangleManager::new(&mut canvas);

        triangle_manager.randomize_vertices();
        triangle_manager.draw_triangles();
    }
}
